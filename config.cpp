#include "config.hpp"
#include <modules/debug-utils/debug-utils.hpp>
#include <Arduino.h>
#include <modules/app-base/application.hpp>

char *cfg_mqttBaseTopic = nullptr;
uint16_t cfg_mqttBaseTopicLength = 0;
char *cfg_mqttServer = nullptr;
uint16_t cfg_mqttServerLength = 0;
uint16_t cfg_mqttPort = 0;

void CFG_loadDefaults() {
	String dummy = String("esp/" + String( Application::getChipID() ) + "\0");
	CFG_setMqttBaseTopic( dummy.c_str(), dummy.length() );
	CFG_setMqttServer( "192.168.1.1", 11 );
	CFG_setMqttPort(1883);
}

void CFG_setMqttBaseTopic(const char *str, uint8_t len) {
	if(cfg_mqttBaseTopic != nullptr) {
		free(cfg_mqttBaseTopic);
	}
	cfg_mqttBaseTopic = (char *) malloc(len + 1);
	memset(cfg_mqttBaseTopic, 0, len + 1);
	memcpy(cfg_mqttBaseTopic, str, len);
	cfg_mqttBaseTopicLength = len;
}

char* CFG_getMqttBaseTopic() {
	return cfg_mqttBaseTopic;
}

void CFG_setMqttServer(const char *str, uint8_t len) {
	if(cfg_mqttServer != nullptr) {
		free(cfg_mqttServer);
	}
	cfg_mqttServer = (char *) malloc(len + 1);
	memset(cfg_mqttServer, 0, len + 1);
	memcpy(cfg_mqttServer, str, len);
	cfg_mqttServerLength = len;
}

char* CFG_getMqttServer() {
	return cfg_mqttServer;
}

void CFG_setMqttPort(uint16_t port) {
	cfg_mqttPort = port;
}

uint16_t CFG_getMqttPort() {
	return cfg_mqttPort;
}
