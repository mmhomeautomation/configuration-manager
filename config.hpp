#ifndef __CONFIG_HPP__
#define __CONFIG_HPP__

#include <inttypes.h>

void CFG_loadDefaults(void);

void CFG_setMqttServer(const char* str, uint8_t length);
char* CFG_getMqttServer(void);
uint16_t CFG_getMqttServerLength(void);

void CFG_setMqttBaseTopic(const char* str, uint8_t length);
char* CFG_getMqttBaseTopic(void);
uint16_t CFG_getMqttBaseTopicLength(void);

void CFG_setMqttPort(uint16_t port);
uint16_t CFG_getMqttPort();

#endif
